freezer (15.0.0-2) unstable; urgency=medium

  * Removed python3-mock from build-depends (Closes: #1068967).

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Apr 2024 08:40:43 +0200

freezer (15.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uplaoding go unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Oct 2023 10:12:19 +0200

freezer (15.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Cleans better.

 -- Thomas Goirand <zigo@debian.org>  Thu, 14 Sep 2023 10:25:15 +0200

freezer (14.0.0-3) unstable; urgency=medium

  * Cleans properly (Closes: #1044531).

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Aug 2023 16:50:40 +0200

freezer (14.0.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Removed obsolete lsb-base depends.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 14:06:14 +0200

freezer (14.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Mar 2023 15:16:09 +0100

freezer (14.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 01 Mar 2023 16:10:49 +0100

freezer (13.0.0-2) unstable; urgency=medium

  * Fix python3 shebang (Closes: #1030269).

 -- Thomas Goirand <zigo@debian.org>  Tue, 14 Feb 2023 14:21:02 +0100

freezer (13.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2022 08:34:44 +0200

freezer (13.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Sep 2022 18:23:17 +0200

freezer (13.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Sep 2022 09:22:56 +0200

freezer (12.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 16:12:14 +0200

freezer (12.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 26 Mar 2022 10:13:06 +0100

freezer (12.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Sat, 12 Mar 2022 18:10:13 +0100

freezer (11.0.0-2) unstable; urgency=medium

  * Added spanish translation of the debconf template thanks to Camaleón
    (Closes: #988378).
  * Added portuguese translation of the debconf template thanks to Américo
    Monteiro (Closes: #982751).

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Dec 2021 11:43:40 +0100

freezer (11.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 17:06:20 +0200

freezer (11.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 23:54:58 +0200

freezer (11.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 Sep 2021 10:34:53 +0200

freezer (10.0.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 12:43:03 +0200

freezer (10.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Apr 2021 19:03:06 +0200

freezer (10.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fix b-d.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Mar 2021 14:30:10 +0100

freezer (9.0.0-2) unstable; urgency=medium

  * Added initial debconf templates translations:
    - fr.po thanks to Jean-Pierre Giraud (Closes: #969724).
    - nl.po thanks to Frans Spiesschaert (Closes: #969343).
    - de.po thanks to Helge Kreutzmann (Closes: #968855).

 -- Thomas Goirand <zigo@debian.org>  Mon, 23 Nov 2020 09:56:44 +0100

freezer (9.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.
  * Add import unittest in feature-cindernative-return-cinder-backup-id.patch.

 -- Thomas Goirand <zigo@debian.org>  Sat, 17 Oct 2020 22:38:17 +0200

freezer (9.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Refresh feature-cindernative-return-cinder-backup-id.patch.

 -- Thomas Goirand <zigo@debian.org>  Fri, 02 Oct 2020 14:43:23 +0200

freezer (8.0.0-2) unstable; urgency=medium

  * Add fix-jenkins-build.patch
  * Add feature-cindernative-return-cinder-backup-id.patch

 -- Michal Arbet <michal.arbet@ultimum.io>  Wed, 12 Aug 2020 16:18:54 +0200

freezer (8.0.0-1) unstable; urgency=medium

  * Initial release (Closes: #966567).

 -- Michal Arbet <michal.arbet@ultimum.io>  Fri, 31 Jul 2020 14:57:56 +0200
